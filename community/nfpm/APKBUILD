# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=nfpm
pkgver=2.39.0
pkgrel=0
pkgdesc="a simple apk, Deb and RPM packager without external dependencies"
url="https://nfpm.goreleaser.com/"
arch="all !riscv64"
license="MIT"
options="net" # net for go
makedepends="go"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/goreleaser/nfpm/archive/v$pkgver.tar.gz"

# secfixes:
#   2.35.2-r0:
#     - CVE-2023-49568

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "-X main.version=$pkgver" -v -o nfpm ./cmd/nfpm/main.go
}

check() {
	go test
}

package() {
	install -Dm0755 nfpm "$pkgdir"/usr/bin/nfpm
	install -Dm0644 LICENSE.md "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
bac36520ee44ace628b6a04f11d1cb3b2033042cfdb1aa888b02a7dab0a3ea34c4889dcb5795d6971e9268dc62b7e40852ff7e40472e787b695737e6b06bd284  nfpm-2.39.0.tar.gz
"
