# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-boto3
pkgver=1.35.6
pkgrel=0
pkgdesc="AWS SDK for Python (Boto3)"
url="https://aws.amazon.com/sdk-for-python/"
license="Apache-2.0"
arch="noarch"
depends="
	py3-botocore
	py3-jmespath
	py3-s3transfer
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/boto3/boto3-$pkgver.tar.gz"
builddir="$srcdir"/boto3-$pkgver
options="!check" # take way too long

replaces="py-boto3" # Backwards compatibility
provides="py-boto3=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
9704c86838add1fb2ed84f0966294acc93e6fe49ce4872987bbb53a7775cb1db7bce24a27aefb201cfd13f5d170c98cd6dda74504626291595ca255429d49020  boto3-1.35.6.tar.gz
"
