# Contributor: mekyt <hello@mek.yt>
# Maintainer: mekyt <hello@mek.yt>
pkgname=zulip-desktop
pkgver=5.11.0
pkgrel=0
pkgdesc="Desktop application for Zulip, an open source chat and collaborative software"
url="https://github.com/zulip/zulip-desktop"
# same as electron
arch="aarch64 x86_64"
license="Apache-2.0"
depends="
	electron
"
makedepends="
	electron-tasje
	nodejs
	npm
	pnpm
"
options="net !check"

source="
	$pkgname-$pkgver.tar.gz::https://github.com/zulip/zulip-desktop/archive/refs/tags/v$pkgver.tar.gz
	disable-auto-update.patch
	$pkgname
	$pkgname.desktop
"

build() {
	pnpm install --ignore-scripts
	pnpm dlx vite build
	pnpm prune --prod --ignore-scripts

	tasje pack
}

package() {
	cd "$srcdir"

	install -Dm644 $pkgname.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname

	cd "$builddir"

	install -Dm644 tasje_out/resources/app.asar "$pkgdir"/usr/lib/$pkgname/app.asar

	while read -r size; do
		install -Dm644 tasje_out/icons/$size.png "$pkgdir"/usr/share/icons/hicolor/$size/apps/zulip.png
	done < tasje_out/icons/size-list
}

sha512sums="
b720f585e471b5aca8b2a890dadd4586e29b58f8323b1f9a6eb382be4acdd82b3dbf7ea095bf274b55efb4d071bf78f3eac883ccbbe1910549bbbcc556d06d8e  zulip-desktop-5.11.0.tar.gz
b0d9ef3a0b431c63a934f2908bc10db2e3a343db008b8f1c5415d6e7c1485e1c30f0f63b6dabae8c3d705c2499958ed0b8a8775c093326f380b4b2c23730c7a7  disable-auto-update.patch
9e2d8aa4cfde943c3d8827fb36511848902852f55fe8d6899c0097a0f15ad37e0c5f0e0b6d530cbfda2f6aacd5d77e4cfb5c2a4438ec34bf174bca41cb25a2a7  zulip-desktop
a893639364d2b972de7b29c7ea7399a79d818cf9dc0f3df7c6aaa9e349803fa187f7030c12ae81881f7f2eef9d2aecca22a9e1bf0b7198726a993ea6a5f368ed  zulip-desktop.desktop
"
