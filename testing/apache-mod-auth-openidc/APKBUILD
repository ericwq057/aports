# Maintainer: Takumi Takahashi <takumiiinn@gmail.com>
pkgname=apache-mod-auth-openidc
_pkgname=mod_auth_openidc
pkgver=2.4.16
pkgrel=0
pkgdesc="OpenID Connect Relying Party implementation for Apache HTTP Server 2.x"
url="https://www.mod-auth-openidc.org"
arch="all !s390x" # tests fails on s390x from cjose
license="Apache-2.0"
depends="apache2"
makedepends="
	apache2-dev
	cjose-dev
	curl-dev
	hiredis-dev
	jansson-dev
	jq-dev
	musl-dev
	oniguruma-dev
	openssl-dev
	pcre2-dev
	"
subpackages="$pkgname-doc $pkgname-static"
source="https://github.com/OpenIDC/mod_auth_openidc/releases/download/v$pkgver/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver

# secfixes:
#   2.4.15.3-r0:
#     - CVE-2024-24814

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-jq=/usr
	make
}

check() {
	make test
}

package() {
	mkdir -p "$pkgdir"/usr/lib/apache2
	/usr/share/apache2/build/instdso.sh SH_LIBTOOL='/usr/share/build-1/libtool' mod_auth_openidc.la "$pkgdir"/usr/lib/apache2
	/usr/share/build-1/libtool --mode=install install mod_auth_openidc.la "$pkgdir"/usr/lib/apache2

	mkdir -p "$pkgdir"/etc/apache2/conf.d
	cat <<- __EOF__ > "$pkgdir"/etc/apache2/conf.d/mod-auth-openidc.conf
	LoadModule auth_openidc_module mod_auth_openidc.so

	__EOF__
	cat auth_openidc.conf >> "$pkgdir"/etc/apache2/conf.d/mod-auth-openidc.conf

	install -Dm644 README.md "$pkgdir"/usr/share/doc/"$pkgname"/README.md
}

sha512sums="
e0cca71679282f90764ef0fcb01d7579bc9c9fa9954cf36eff6dac33d8b100f473658621f945e4929ab75b5edab7d3e1eb500925f8437e01e9081efe361aba64  mod_auth_openidc-2.4.16.tar.gz
"
