# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=xfsprogs
pkgver=6.10.0
pkgrel=0
pkgdesc="XFS filesystem utilities"
url="https://xfs.org/index.php/Main_Page"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	attr-dev
	bash
	inih-dev
	linux-headers
	python3
	userspace-rcu-dev
	util-linux-dev
	"
options="!check"  # no test suite
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-extra"
source="https://mirrors.edge.kernel.org/pub/linux/utils/fs/xfs/xfsprogs/xfsprogs-$pkgver.tar.xz"

build() {
	export DEBUG=-DNDEBUG
	export OPTIMIZER="$CFLAGS -flto=auto"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/sbin \
		--libexecdir=/usr/lib \
		--localstatedir=/var \
		--enable-gettext=no
	make
}

package() {
	make -j1 DIST_ROOT="$pkgdir" install install-dev
	rm -v "$pkgdir"/usr/lib/libhandle.la
	mv "$pkgdir"/sbin "$pkgdir"/usr
	mkdir -p "$pkgdir"/sbin
	for i in mkfs.xfs fsck.xfs xfs_repair; do
		mv "$pkgdir"/usr/sbin/$i "$pkgdir"/sbin/
	done
	chown -R root:root "$pkgdir"/*
}

extra() {
	depends="$pkgname python3" # python3 for xfs_scrub_all
	pkgdesc="XFS filesystem extra utilities"

	# shellcheck disable=2115
	rm -rf "$pkgdir"/lib "$pkgdir"/usr/lib
	amove usr/
}

sha512sums="
ec80eedfd3471c836e99eb8507c0de2895f261d36316145b02f2effbcc1bb5a52eae3ad2148d45bc49f1a30e0267aa4e3f3176a5e01ec84b2da24b3fb430ffce  xfsprogs-6.10.0.tar.xz
"
